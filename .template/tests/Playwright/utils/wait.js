export class Wait {
  static async nextXthMinute(minute = 5) {
    const minutesInMs = 1000 * 60 * minute;
    const waitInMs = minutesInMs - (new Date().getTime() % minutesInMs)

    return new Promise(resolve => setTimeout(resolve, waitInMs + (minute * 1000)));
  }
}
