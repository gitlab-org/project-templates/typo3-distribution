# Template information

This is a GitLab project template.
For a overview of all available project templates see
https://gitlab.com/gitlab-org/project-templates/

## Working in a fork

For using CI in a fork a custom path for the "CI/CD configuration file" setting
must be set.

In order to get it to work, go to "Settings -> CI/CD -> General pipelines".
Expand "General pipelines" and enter `.template/gitlab-ci-project-template.yml`
in "CI/CD configuration file" setting.

By default, the GitLab Runner uses the `.gitlab-ci.yml` file for CI.
Setting "CI/CD configuration file" enables the run the actual template
tests.

The `deploy_to_*` jobs require `$SSH_PRIVATE_KEY` variable set under
"Settings -> CI/CD -> Variables".

Click on "Add variable" and use `SSH_PRIVATE_KEY` as "key".
To generate a SSH key pair run the following command and copy
the content of the generated file (the one without `.pub` at the end)
into the "value" field and click save.

```bash
ssh-keygen -C "Gitlab TYPO3 Template" -f ci-test
```

This key pair is only for testing purposes.
Do not use it anywhere else.

## Update to new major version

To get ready for a new TYPO3 major release,
it is necessary to update all package names
starting with `typo3/cms-`

Step by step:

1. Set new version in `composer.json` for all `typo3/cms-*`
2. Update `platform:php` `composer.json` according to the requirements
3. Update `php_version` in `.ddev/config.yaml` according to the requirements

## Run Playwright tests

In the root of the project run the following commands
to prepare DDEV:

```
export TEST_DIR=.template/tests/Playwright
ddev exec "cd $TEST_DIR && npm install"
ddev exec "cd $TEST_DIR && npx playwright install --with-deps"
```

Run tests within DDEV:
```
ddev exec "cd $TEST_DIR && ./node_modules/.bin/playwright test"
```

Run tests locally with a GUI:
```
cd .template/tests/Playwright
/node_modules/.bin/playwright test --ui
```

## Update project template in GitLab

Usually done by GitLab staff

Setup GDK according to the [docs](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation)
Ensure GDK is up running as expected (https://127.0.0.1:3000/)

```
gdk start
```

To export and download the repository as tar.gz and store it in `gitlab-development-kit/gitlab/vendor/project_templates/typo3_distribution.tar.gz`
run the following command:
```
bundle exec rake gitlab:update_project_templates\[ochorocho/typo3-distribution\]
```

See [details](https://docs.gitlab.com/ee/development/project_templates.html#test-your-built-in-project-with-the-gitlab-development-kit)
